<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Fichier produit par PlugOnet
// Module: paquet-agregateurs_sql
// Langue: fr
// Date: 29-07-2024 16:27:32
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'agregateurs_sql_description' => 'Ajout des expressions, notamment d\'agrégation, dans le select sql : count(), sum(), max(), etc...',
	'agregateurs_sql_slogan' => 'Ajout des expressions, notamment d\'agrégation, dans le select sql',
);
?>