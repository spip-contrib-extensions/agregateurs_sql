# Agrégateurs SQL

Criteres supplémentaires d'agrégation SQL


Featuring : Compteurs et agrégation sql (calculs statistiques).


## Balise `#EXPRESSION`

La nouvelle balise `#EXPRESSION{expression}` permet d'ajouter des expressions sql au select.

`#EXPRESION{expression,alias}` permet de trier `{par alias}`


### Critères d'agrégation dans les boucles avec le critère `{fusion}`

```
<BOUCLE_art(ARTICLES){fusion statut}>
#EXPRESSION{expression}
</BOUCLE_art>
```

Somme des id_article de statut publie
  * `#EXPRESSION{SUM(id_article)}` = 37. Equivalent à `#SUM{id_article}` qui vaut aussi 37

Autres agrégateurs
  * `#EXPRESSION{COUNT(id_article)}` = 8 - nombre d'id_article de statut publie
  * `#EXPRESSION{AVG(id_article)}` = 4.6250 - moyenne des id_article de statut publie
  * `#EXPRESSION{MIN(id_article)}` = 1 - minimum des id_article de statut publie
  * `#EXPRESSION{MAX(id_article)}` = 9 - maximum des id_article de statut publie


### Exemple d'usage de la balise #EXPRESSION avec tri

#### Trier les articles en fonction de leur nombre de mots clés :

```
<ul>
<BOUCLE_articles(ARTICLES spip_mots_articles){id_mot >0}{fusion articles.id_article}{!par compteur_mots}{0,10}>
	<li>#ID_ARTICLE - [(#EXPRESSION{COUNT(id_mot),compteur_mots})] mots-clés</li>
</BOUCLE_articles>
</ul>
```

#### Trier les articles en fonction de leur nombre de commentaires :

````
<ul>
<BOUCLE_articles(FORUMS){fusion id_article}{!par compteur_messages}{0,10}>
	<li><BOUCLE_article(ARTICLES){id_article}>#TITRE</BOUCLE_article> - [(#EXPRESSION{COUNT(id_forum),compteur_messages})] commentaires</li>
</BOUCLE_articles>
</ul>
````

#### Trier les rubriques en fonction de leur nombre d'articles, et faire un commentaire en fonction du nombre d'articles :

````
<BOUCLE_art(ARTICLES){fusion id_rubrique}{!par nombre_articles}>
Rubrique : #EXPRESSION{COUNT(id_article), nombre_articles} art.
Evaluation littérale : #EXPRESSION{CASE COUNT(id_article) WHEN 1 THEN 'one' WHEN 2 THEN 'two' ELSE 'more' END}
</BOUCLE_art>
````

```text
Rubrique 3 : 7 art.
    Evaluation littérale : more
Rubrique 1 : 1 art.
    Evaluation littérale : one
```

Voir tous les agregateurs possibles : http://dev.mysql.com/doc/refman/5.1/en/control-flow-functions.html


#### Le cas du tri des articles par nombre de commentaires.

Contre toute attente, la boucle

```
<BOUCLE_articles(articles spip_forum){id_forum >0}{articles.id_article=L2.id_article}{fusion articles.id_article}{!par compteur_messages}{0,10}>
    <li>#ID_ARTICLE - #EXPRESSION{COUNT(id_forum),compteur_messages} commentaires</li>
</BOUCLE_articles>
```

génère

```
SELECT articles.id_article, COUNT(id_forum) as compteur_messages, articles.lang, articles.titre
FROM spip_articles AS `articles`
INNER JOIN spip_auteurs_articles AS L1 ON ( L1.id_article = articles.id_article )
INNER JOIN spip_forum AS L2 ON ( L2.id_auteur = L1.id_auteur )
WHERE (articles.statut = 'publie')
	AND (articles.date < '2037-11-12 20:23:40')
	AND (L2.id_forum > 0)
	AND (articles.id_article = 'L2.id_article')
GROUP BY articles.id_article,articles.id_article
ORDER BY compteur_messages DESC
LIMIT 0,10
```

Ce qui ne permet pas d'arriver au résultat voulu.

en revanche on peut le faire dans l'autre sens :

```
<ul>
<BOUCLE_articles(FORUMS spip_articles){articles.id_article >0}{fusion forum.id_article}{!par compteur_messages}{0,10}>
	<li>[(#EXPRESSION{L1.titre})] - [(#EXPRESSION{COUNT(id_forum),compteur_messages})] commentaires</li>
</BOUCLE_articles>
</ul>
```
