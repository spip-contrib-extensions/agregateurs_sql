<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Balise `#SUM`
 * @param $p
 * @return mixed
 */
function balise_SUM($p) {
	$t = $p->param[0][1][0]->texte;
	$p->param[0][1][0]->texte = "SUM($t)";
	$p->param[0][2][0]->texte = "sum_$t";
	return balise_EXPRESSION($p);
}

/**
 * Balise `#EXPRESSION`
 *
 * @param $p
 * @return mixed
 */
function balise_EXPRESSION($p) {
	static $num = 1;
	$b = $p->nom_boucle ? $p->nom_boucle : $p->descr['id_mere'];
	if ($b === '' || !isset($p->boucles[$b])) {
		erreur_squelette(
			_T(
				'zbug_champ_hors_boucle',
				['champ' => "#$b" . 'EXPRESSION']
			),
			$p->id_boucle
		);
		$p->code = "''";
	} else {
		if (
			isset($p->param[0][1][0])
			and $champ = ($p->param[0][1][0]->texte)
		) {
			if (isset($p->param[0][2][0]) and $p->param[0][2][0]->texte) {
				$alias = $p->param[0][2][0]->texte;
			} else {
				$alias = "expr_$num";
				$num++;
			}
			$p->code = "\$Pile[\$SP]['$alias']";
			$p->boucles[$b]->select[] = "$champ as $alias";
			$p->interdire_scripts = true;
		} else {
			erreur_squelette(
				'pas de balises dans #EXPRESSION',
				$p->id_boucle
			);
			$p->code = "''";
		}
	}
	return $p;
}

/**
 * Critere {is_null champ} - Franck Van Lancker -
 */
function critere_is_null($idb, &$boucles, $crit) {

	$not = ($crit->not ? ' NOT' : '');
	$param = "IS$not NULL";
	$boucle = &$boucles[$idb];
	$field = $crit->param[0][0]->texte;

	if (preg_match('~\s~si', $field)) {
		erreur_squelette(_T('zbug_info_erreur_squelette'), $crit->op);
	}

	$boucle->where[] = ["'$param'", "'$boucle->id_table." . "$field'", "''"];
}
